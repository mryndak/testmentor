package fo;

import java.math.BigDecimal;

/**
 * Immutable class
 */
public final class Transaction {

    private final Long id;
    private final BigDecimal ammout;
    private final String name;

    public Transaction(Long id, BigDecimal ammout, String name) {
        this.id = id;
        this.ammout = ammout;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public BigDecimal getAmmout() {
        return ammout;
    }

    public String getName() {
        return name;
    }
}
