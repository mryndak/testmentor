package fo.zad1;

import fo.Transaction;
import fo.User;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {
        zad1();
        zad2();
    }

    public static void zad1() {
        User janusz = new User("Janusz");
        User kamil = new User("Kamil");
        User janusz2 = new User("Janusz");
        List<User> userList = new ArrayList<>();
        userList.add(janusz);
        userList.add(kamil);
        userList.add(janusz2);

        List<User> januszUserList = userList.stream().filter(user -> user.getName().equals("Janusz")).collect(Collectors.toList());
        januszUserList.forEach( u -> System.out.println(u.getName()));
    }

    public static void zad2() {
        User user = User.UserBuilder.createUser()
                .withId(1L)
                .withName("Mateusz")
                .withLastName("Ryndak")
                .withDateOfBirth(LocalDate.now())
                .build();

    }

    public static void zad3() {
        /*
        Klucz musi być uniklany może to być inny obiekt np. String lub primityw np. enum,
        Drugi argument Object musi być to obiekt lub interfejs np. (Funcition, BiFuncion) czy też mapa itp.
         */
    }

    public static void zad4() {
        Transaction transaction = new Transaction(1L, new BigDecimal(123.22), "przelew");
    }
}