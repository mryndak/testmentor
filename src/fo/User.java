package fo;

import java.time.LocalDate;

public class User {

    private Long id;
    private String name;
    private String lastName;
    private LocalDate dateOfBirth;

    public User(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }


    public static final class UserBuilder {
        private Long id;
        private String name;
        private String lastName;
        private LocalDate dateOfBirth;

        private UserBuilder() {
        }

        public static UserBuilder createUser() {
            return new UserBuilder();
        }

        public UserBuilder withId(Long id) {
            this.id = id;
            return this;
        }

        public UserBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public UserBuilder withLastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public UserBuilder withDateOfBirth(LocalDate dateOfBirth) {
            this.dateOfBirth = dateOfBirth;
            return this;
        }

        public User build() {
            User user = new User(name);
            user.dateOfBirth = this.dateOfBirth;
            user.id = this.id;
            user.lastName = this.lastName;
            return user;
        }


    }


}